1. the order is cubic for all 3, as $n^3$ is in each leading term.
2. the order is cubic because the leading term is $n^3$
3. $af+b$ is not in $O(g)$
4. $f_1 + f_2$ are not in $O(g)$
5. ?
6. ? \
**Sorting**
1. A comparison sort is an algorithim that sorts elements based on simple comparisons, like greater than, equal to, or less than statements.
2. O($n^2$), it is definitely the wrong way to go because it is inefficient and slow unless the list is already sorted.
3. 