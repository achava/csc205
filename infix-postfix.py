class ExpressionTree():
    def IsOperator(self, s):
        if "+" in s:
            return True
        if "-" in s:
            return True
        if "*" in s:
            return True
        if "/" in s:
            return True
        return False
    
    def infix_to_postfix(self, infix):
            stack = []
            postfix = ""
            priority = {'+':1, '-':1, '*':2, '/':2}

            for char in infix:
                # if char is "("
                if char == "(":
                    stack.append(char)

                # if char is ")"
                elif char == ")": 
                    while stack and stack[-1] != "(":
                        postfix += stack.pop()
                    stack.pop()

                # if char is an operand
                elif not self.IsOperator(char) and char != "(" and char != ")":
                    postfix += char
                
                # char is an operator
                else: 
                    while stack and stack[-1] != "(" and priority[char] <= priority[stack[-1]]:
                        postfix += stack.pop()
                    stack.append(char)
                
            while stack:
                postfix += stack.pop()
            return postfix
    
    def infix_to_prefix(self, infix):

        for char in infix:
            l = len(infix)
 
        infix = infix[::-1]
    
        for i in range(l):
            if infix[i] == '(':
                infix[i] = ')'
            elif infix[i] == ')':
                infix[i] = '('
    
        prefix = infix_to_postfix(infix)
        prefix = prefix[::-1]
    
        return prefix



tree = ExpressionTree()


# test cases
a = "(2*(3/1))+4"
b = "((3*2)-1)*2"
c = "3*(1-2)-(8-2)*(1+3)"

print(tree.infix_to_postfix(a))
print(tree.infix_to_postfix(b))
print(tree.infix_to_postfix(c))
