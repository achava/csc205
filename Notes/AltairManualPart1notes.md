Part 1 Introduction
- Anyone can use the Altair 8800
- Part 1 will talk about the basics including terms and computer logic, 
- Part 2 and 3 will talk about the organization and operation of the computer
- Part 4 will talk about the computer's 78 instructions
Logic
- Boole deduced that any logical statement could be reasoned with aritmetic. This is where boolean algebra came from, which is essential to programming based off of existing conditions.
- Different permutations of 1s and 0s make up the 3 basic functions: AND OR and NOT
- By plotting the results of A and B as 0 or 1 on a table, you can determine every outcome. These are called Truth Tables
Electronic Logic
- You can implement these on circuits. Half circle for AND, crescent for OR, triangle for OUT
- Combine the circuits together to get more statements, or a logic system 
Number Systems
- Binary, Octal, Hexadecimal. Humans are used to base 10
Binary System
- The ALtair is almost all binary, using 1s and 0s to form bytes.
