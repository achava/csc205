#include <iostream>
using namespace std;

void return_square(int num) {
    int sqr = num * num;
    cout << sqr; 
}

int main() {
    return_square(2); 
    return 0;
}
// ld: can't link with a main executable file 'func_results' for architecture arm64