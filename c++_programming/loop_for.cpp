#include <iostream>
using namespace std;

void loopForever(int n) {
    while (true) {
        cout << "n is now " << n << "." << endl;
        n = n + 1;
    }
}

int main() {
    loopForever(5); // starts at n = 5, adds 1 in a loop
}

// What do you think the call stack has to do with the outcome of your experiment with these two functions?
// In this program, the top of the stack is the loopForever function. It is never popped off, as the while loop will always remain True.