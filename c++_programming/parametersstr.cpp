#include <iostream>
using namespace std;

void print_twice(char phil) {
    cout << phil << phil << endl;
}

int main() {
    print_twice('a b c'); //warning: multi-character character constant [-Wmultichar]
    return 0;
}