#include <iostream>
#include <math.h>
using namespace std;

void tables() {
    double x = 2.0;
    while (x <= 16) {
        cout << pow(2, x) << endl;
        x = x+1;
    }
}


int main() {
    tables();
}
/*
4
8
16
32
64
128
256
512
1024
2048
4096
8192
16384
32768
65536
*/
