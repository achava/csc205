#include <iostream>
#include <string>
#include <math.h>
using namespace std;

struct Point {
    double x, y;
};

struct Rectangle {
    Point corner;
    double width, height;
};



Point find_rectanglepoint(Rectangle& r) { // the point with the highest x value and the lowest y value
    Point lowerRight = {r.corner.x + r.width, r.corner.y};
    return lowerRight;
}

int main() {
    Rectangle test = {{0,0}, 10.0, 20.0};
    Point p = find_rectanglepoint(test);
    cout << "coordinates of lower right hand corner: " << "(" << p.x << "," << p.y << ")";
}