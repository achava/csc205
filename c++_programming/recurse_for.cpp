#include <iostream>
using namespace std;

void recurse_forever(int n) {
    cout << "n is now " << n << "." << endl;
    recurse_forever(n + 1);
}

int main() {
    recurse_forever(5); // n = 5, and the function is called infinitely
}

//  What do you think the call stack has to do with the outcome of your experiment with these two functions?
// In this program, the recurse_forever function is constantly added to the top of the stack, and then popped off. The computer does this until the programmer exits the program