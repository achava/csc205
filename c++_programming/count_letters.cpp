#include <iostream>
#include <string>
using namespace std;

void countLetters(string state, char letter) {
    int count = 0;
    int index = 0;

    while (index < state.length()) {
        if (state[index] == letter) {
            count = count + 1;
        }
        index = index + 1;
    }
    cout << count << endl;
}

int main() {
    countLetters("arkansas", 'a');
}