#include <iostream>
#include <string>
using namespace std;

struct Node {
    int cargo;
    Node* next;

    Node() {
        cargo = 0;
        next = NULL;
    }

    Node(int cargo, Node* next) {
        this->cargo = cargo;
        this->next = next;
    }

    string to_str() const {
        return to_string(cargo);
    }
};

Node* remove_second(Node* list) { // if first is null or second is null, return null
    Node* first = list;
    if (list->next == NULL) {
        return NULL;
    }
    Node* second = list->next;


    // make the first node point to the third
    first->next = second->next;

    // remove the second node from the list and return a pointer to it
    second->next = NULL;
    return second;
}
// The function remove_second presented in Modifying linked lists will crash 
// if passed a singleton or empty list. Fix it so that it returns a NULL in 
// these cases instead.

void print_list(Node* list) {
    Node* node = list;
    cout << "(";
    while (node->next != NULL) { //checks to see if the node after the current one is null. if it is null, it will break and end the list
        cout << node->cargo << ", ";
        node = node->next;
    }
    cout << node->cargo << ")";
    // while (node != NULL) {
    //    cout << node->cargo;
    //    node = node->next;
    //    if (node != NULL) cout << ", ";
    //} OLD CODE
}

//^^ By convention, lists are printed with parentheses with commas between the elements,
//as in (1, 2, 3). Modify the print_list function from Lists as collections so that 
//it generates output in this format.

void print_backward(Node* list) {
    if (list == NULL) return;

    Node* head = list;
    Node* tail = list->next;

    print_backward(tail);
    if (head->next != NULL) cout << ", ";
    cout << head->cargo;
}

int main () {
    Node* list = new Node(1, NULL);
    list->next = new Node(2, NULL);
    list->next->next = new Node(3, NULL);
    Node* list2 = new Node(1, NULL);

    cout << "Original list: ";
    print_list(list);
    cout << endl;

    Node* removedNode = remove_second(list);
    cout << "Removed node: " << removedNode->cargo << endl;

    cout << "Updated list: ";
    print_list(list);
    cout << endl;

    cout << "List printed backward: ";
    print_backward(list);
    cout << endl;

    cout << "List with one node: ";
    remove_second(list2);
    cout << endl;
}