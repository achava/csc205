#include <iostream>
#include <string>
using namespace std;

struct Node {
    int cargo;
    Node* next;

    Node() {
        cargo = 0;
        next = NULL;
    }

    Node(int cargo, Node* next) {
        this->cargo = cargo;
        this->next = next;
    }

    string to_str() const {
        return to_string(cargo);
    }
};

Node* remove_second(Node* list) {
    Node* first = list;
    Node* second = list->next; // try catch block?

    // make the first node point to the third
    first->next = second->next;

    // remove the second node from the list and return a pointer to it
    second->next = NULL;
    return second;
}
// The function remove_second presented in Modifying linked lists will crash 
// if passed a singleton or empty list. Fix it so that it returns a NULL in 
// these cases instead.

void print_list(Node* list) {
    Node* node = list;
    Node* node2;
    cout << "(";
    for (Node* node = list; node != NULL; node = node->next) {
        cout << node->cargo;
        node = node->next;
        node2 = node->next;
        if (node != NULL && node2 == NULL) cout << ", ";
    }
    // Rewrite print_list using a for loop instead of a while loop.
    cout << ")";
}

//^^ By convention, lists are printed with parentheses with commas between the elements,
//as in (1, 2, 3). Modify the print_list function from Lists as collections so that 
//it generates output in this format.

void print_backward(Node* list) {
    if (list == NULL) return;

    Node* head = list;
    Node* tail = list->next;

    print_backward(tail);
    if (head->next != NULL) cout << ", ";
    cout << head->cargo;
}

int main () {
    Node* list = new Node(1, nullptr);
    list->next = new Node(2, nullptr);
    list->next->next = new Node(3, nullptr);

    cout << "Original list: ";
    print_list(list);
    cout << endl;

    Node* removedNode = remove_second(list);
    cout << "Removed node: " << removedNode->cargo << endl;

    cout << "Updated list: ";
    print_list(list);
    cout << endl;

    cout << "List printed backward: ";
    print_backward(list);
    cout << endl;
}