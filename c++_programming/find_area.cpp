#include <iostream>
#include <string>
#include <math.h>
using namespace std;

struct Point {
    double x, y;
};

struct Rectangle {
    Point corner;
    double width, height;
};



int find_rectanglearea(Rectangle& r) { // the length times the width is the area
    int rectArea = (r.width * r.height);
    return rectArea;
}

int main() {
    Rectangle test = {{0,0}, 10.0, 20.0};
    int p = find_rectanglearea(test);
    cout << "area of rectangle: " << "(" << p << ")";
}