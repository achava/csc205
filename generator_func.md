## Generator Functions

Up until now in Python, when making functions, we'll use iterators like 
```
print("\nString Iteration")   
s = "Geeks"
for i in s :
    print(i)
```

Output:
```
String Iteration
G
e
e
k
s
```
But, if you define a function like so:

```
def generator():
    yield "one fish"
    yield "two fish"
    yield "red fish"
    yield "blue fish"
```

You write a generator object (keyword `yield`)

Once you initialize it, you can iterate through the relevant yield statement with the `next` function.
```
x = generator()
next(x)

```